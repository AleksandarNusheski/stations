import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class Stations{
	int id, availableDocks, totalDocks, statusKey, availableBikes;
	String stationName, statusValue, stAddress1, stAddress2;
	double latitude, longitude;
	
	public Stations(int id, int availableDocks, int totalDocks, int statusKey, int availableBikes,
	String stationName, String statusValue, String stAddress1, String stAddress2, double latitude, double longitude){
		this.id=id; this.availableDocks=availableDocks; this.totalDocks=totalDocks; this.statusKey=statusKey;
		this.availableBikes=availableBikes; this.stationName=stationName; this.statusValue=statusValue;
		this.stAddress1=stAddress1; this.stAddress2=stAddress2; this.latitude=latitude; this.longitude=longitude;
	}
}

public class NetClientGet {
	public static void main(String[] args) throws JSONException {
		Stations s[]=Parser();
		countInServiceStations(s);
	}
	
	public static Stations[] Parser() throws JSONException{
		String JSONdata=getData();
		JSONObject jsonObject;
		if(JSONdata!=null) jsonObject = new JSONObject(JSONdata);
		else throw new RuntimeException("Failed : null pointer");
		
		JSONArray jsonArray = jsonObject.getJSONArray("stationBeanList");
		Stations s[]=new Stations[jsonArray.length()];
		
		for(int i=0; i<jsonArray.length(); i++){
			JSONObject obj=jsonArray.getJSONObject(i);
			s[i]=new Stations(
				obj.getInt("id"), obj.getInt("availableDocks"), obj.getInt("totalDocks"), obj.getInt("statusKey"),
				obj.getInt("availableBikes"), obj.getString("stationName"), obj.getString("statusValue"),
				obj.getString("stAddress1"), obj.getString("stAddress2"), obj.getDouble("latitude"), obj.getDouble("longitude")
			);
//			System.out.println(s[i].stationName);
		}
		return s;
	}
	
	public static String getData(){
		try {
			URL url = new URL("https://feeds.citibikenyc.com/stations/stations.json");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output="", line;
			while ((line = br.readLine()) != null) {
				output+=(line+"\n");
			}
			conn.disconnect();
			return output;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void countInServiceStations(Stations s[]){
		int sum=0;
		for(int i=0; i<s.length; i++){
			if(s[i].statusValue.equals("In Service")) sum++;
		}
		System.out.println("All stations: "+s.length+", in service stations: "+sum+".");
	}
}